import { Message } from "discord.js";
import { ExampleCommand } from "./example_command";

export const prefix: string = "!";

export interface Command {
    name: string;
    command_data?: {
        auto_del: boolean
    };

    run(message: Message, args: string[]): void;
};

export const commands: Command[] = [
    new ExampleCommand() // register the command
];
