import { Command } from "./base";
import { Message } from "discord.js";

export class ExampleCommand implements Command {
    name = "example";
    command_data = {
        auto_del: true
    }

    run(message: Message, args: string[]) {
        message.channel.send("This is an example!"); // reply
        message.channel.send(`Arguments: ${args.join(", ")}`);
    }
}
