import * as fs from "fs";
import { Client } from "discord.js";
import { prefix, commands, Command } from "./commands/base";

const client: Client = new Client();

client.once("ready", () => console.log(`Client ready as ${client.user.tag}`));

client.on("message", msg => {
    if (msg.author.bot) {
        return; // ignore bot messages
    }

    const raw_message: string = msg.content;

    if (!raw_message.startsWith(prefix)) {
        return;
    }

    const args: string[] = raw_message.includes(" ") ? raw_message.split(" ") : []; // get string args

    for (let i = 0; i < commands.length; i++) {
        let command: Command = commands[i];
        if ((args.length <= 0 ? raw_message : args[0]).toLowerCase() !== prefix + command.name) {
            continue; // ignore
        }
        args.shift(); // remove first argument
        command.run(msg, args);
        if (command.command_data && command.command_data.auto_del) {
            if (msg.deletable) msg.delete();
        }
        break; // found the command
    }
});

//
// read the credentials file
//

const cred_file: string = "files/cred.json";

let cred: any = JSON.parse(fs.readFileSync(cred_file, "utf-8"));
if (!cred) {
    process.exit();
}

let bot_token: string | undefined = cred["bot_token"];

if (bot_token) {
    // not using process.env this time
    client.login(bot_token).catch(() => {
        console.log("Login failed, please try updating the token.");
        cred["bot_token"] = undefined; // undefine the token
        fs.writeFileSync(cred_file, JSON.stringify(cred, null, 2)); // rewrite the file with the changes
        process.exit();
    });
}
